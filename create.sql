CREATE ROLE backend WITH PASSWORD 'backend' LOGIN CREATEDB;

\connect postgres backend;
CREATE DATABASE projekt_baza;
\connect projekt_baza;

CREATE TABLE "stations" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "model" varchar,
  "serial_no" varchar,
  "dld_name" varchar,
  "dld_sig" varchar,
  "started_at" timestamp,
  "datalogger_name" varchar,
  "ip_address" varchar,
  "security_code" varchar,
  "collection_enabled" smallint,
  "collect_base_date" timestamp,
  "collection_interval" timestamp,
  "retry_interval" timestamp,
  "number_of_retries" smallint,
  "last_collection_date" timestamp,
  "next_collection_date" timestamp,
  "retries_left" smallint
);

CREATE TABLE "station_times" (
  "id" SERIAL PRIMARY KEY,
  "station_id" integer,
  "station_time" timestamp,
  "utc_time" timestamp,
  "response_time" real
);

CREATE TABLE "tables" (
  "id" SERIAL PRIMARY KEY,
  "station_id" integer,
  "name" varchar,
  "started_at" timestamp,
  "ended_at" timestamp,
  "is_active" smallint,
  "collection_enabled" smallint
);

CREATE TABLE "fields" (
  "id" SERIAL PRIMARY KEY,
  "table_id" integer,
  "column_number" smallint,
  "name" varchar,
  "type" varchar,
  "unit" varchar,
  "process" varchar,
  "string_len" smallint
);

CREATE TABLE "sample_times" (
  "id" SERIAL PRIMARY KEY,
  "table_id" integer,
  "time" timestamp,
  "sample_number" bigint
);

CREATE TABLE "samples_float" (
  "id" SERIAL PRIMARY KEY,
  "field_id" integer,
  "sample_time_id" integer,
  "value" double precision
);

CREATE TABLE "samples_long" (
  "id" SERIAL PRIMARY KEY,
  "field_id" integer,
  "sample_time_id" integer,
  "value" bigint
);

CREATE TABLE "samples_timestamp" (
  "id" SERIAL PRIMARY KEY,
  "field_id" integer,
  "sample_time_id" integer,
  "value" timestamp
);

CREATE TABLE "samples_string" (
  "id" SERIAL PRIMARY KEY,
  "field_id" integer,
  "sample_time_id" integer,
  "value" varchar
);

ALTER TABLE "station_times" ADD FOREIGN KEY ("station_id") REFERENCES "stations" ("id");

ALTER TABLE "tables" ADD FOREIGN KEY ("station_id") REFERENCES "stations" ("id");

ALTER TABLE "fields" ADD FOREIGN KEY ("table_id") REFERENCES "tables" ("id");

ALTER TABLE "sample_times" ADD FOREIGN KEY ("table_id") REFERENCES "tables" ("id");

ALTER TABLE "samples_float" ADD FOREIGN KEY ("field_id") REFERENCES "fields" ("id");

ALTER TABLE "samples_float" ADD FOREIGN KEY ("sample_time_id") REFERENCES "sample_times" ("id");

ALTER TABLE "samples_long" ADD FOREIGN KEY ("field_id") REFERENCES "fields" ("id");

ALTER TABLE "samples_long" ADD FOREIGN KEY ("sample_time_id") REFERENCES "sample_times" ("id");

ALTER TABLE "samples_timestamp" ADD FOREIGN KEY ("field_id") REFERENCES "fields" ("id");

ALTER TABLE "samples_timestamp" ADD FOREIGN KEY ("sample_time_id") REFERENCES "sample_times" ("id");

ALTER TABLE "samples_string" ADD FOREIGN KEY ("field_id") REFERENCES "fields" ("id");

ALTER TABLE "samples_string" ADD FOREIGN KEY ("sample_time_id") REFERENCES "sample_times" ("id");

---------------------------------------------------------------------------

INSERT INTO stations (name, model, serial_no, ip_address) VALUES ('Projekt test', 'CR1000X', '386', '161.53.17.95:8080');