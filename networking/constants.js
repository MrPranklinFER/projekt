module.exports.emitCodes = {
    tablesReceived: 'tablesReceived',
    stationInfoReceived: 'stationInfoReceived',
    tableDataReceived: 'tableDataReceived',
    stationTimeReceived: 'stationTimeReceived'
};

module.exports.dataTypes = {
    INTEGER: "xsd:int",
    DOUBLE: "xsd:double",
    FLOAT: "xsd:float",
    STRING: "xsd:string",
    BOOLEAN: "xsd:boolean",
    UNSIGNED_SHORT: "xsd:unsignedShort",
    UNSIGNED_INT: "xsd:unsignedInt",
    DATE_TIME: "xsd:dateTime",
}