const EventEmitter = require('events');
const request = require('request');
const constants = require('./constants');
const Station = require('../model/station');
class Networking extends EventEmitter {

    _onTablesReceived(err, res, body, station) {
        if (err) {
            console.error(err)
        } else {
            this.emit(constants.emitCodes.tablesReceived, { body: body, station: station })
        }
    }

    _onStationInfoReceived(err, res, body, ipAddress) {
        if (err) {
            console.error(err)
        } else {
            let station = new Station(
                0,
                body.head.environment.station_name,
                body.head.environment.model,
                body.head.environment.serial_no,
                Date.now(),
                ipAddress
            );
            this.emit(constants.emitCodes.stationInfoReceived, { body: body, station: station })
        }
    }

    _onTableDataReceived(err, res, body, table, sinceSample) {
        if (err) {
            console.error(err)
        } else {
            this.emit(constants.emitCodes.tableDataReceived, { body: body, table: table, sinceSample: sinceSample })
        }
    }

    _onStationTimeReceived(err, res, body, station, requestTime) {
        if (err) {
            console.error(err)
        } else {
            let currentTime = new Date()
            let responseTime = currentTime.getMilliseconds() - requestTime.getMilliseconds()
            this.emit(constants.emitCodes.stationTimeReceived, { body: body, station: station, requestTime: requestTime, responseTime: responseTime })
        }
    }

    async getListOfTables(station) {
        request('http://' + station.ipAddress, {
            qs: {
                command: "browseSymbols",
                format: "json",
                mode: "most-recent"
            },
            json: true
        }, (err, res, body) => this._onTablesReceived(err, res, body, station))
    }

    async getStationInfo(ipAddress, properties) {
        request('http://' + ipAddress, { qs: properties, json: true }, (err, res, body) => this._onStationInfoReceived(err, res, body, ipAddress))
    }

    async getDataFromTable(table, ipAddress, sinceSample) {
        if (sinceSample === null || typeof sinceSample === 'undefined') {
            request("http://" + ipAddress, {
                qs: {
                    command: "dataquery",
                    uri: table.name,
                    format: "json",
                    mode: "most-recent",
                    p1: 100
                }, json: true
            }, (err, res, body) => this._onTableDataReceived(err, res, body, table))
        } else {
            console.log(`last collection from ${table.name} was number ${sinceSample}`)

            request("http://" + ipAddress + ":8080", {
                qs: {
                    command: "dataquery",
                    uri: table.name,
                    format: "json",
                    mode: "since-record",
                    p1: sinceSample,
                }, json: true
            }, (err, res, body) => this._onTableDataReceived(err, res, body, table, sinceSample))
        }
    }

    async getStationTime(station) {
        let requestTime = new Date()
        request("http://" + station.ipAddress + ":8080", {
            qs: {
                command: "clockcheck",
                format: "json",
            }, json: true
        }, (err, res, body) => this._onStationTimeReceived(err, res, body, station, requestTime))
    }
}

module.exports = Networking;