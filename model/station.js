class Station {
    constructor(id, name, model, serialNo, startedAt, ipAddress) {
        this.id = id;
        this.name = name;
        this.model = model;
        this.serialNo = serialNo;
        this.startedAt = startedAt;
        this.ipAddress = ipAddress
    }
}

module.exports = Station;