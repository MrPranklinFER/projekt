class Field {
    constructor(id, tableId, columnNumber, name, type, unit, process, settable, stringLen) {
        this.id = id;
        this.tableId = tableId;
        this.columnNumber = columnNumber;
        this.name = name;
        this.type = type;
        this.unit = unit;
        this.process = process;
        this.settable = settable;
        this.stringLen = stringLen;
    }
}

module.exports = Field;