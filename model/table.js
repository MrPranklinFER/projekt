class Table {
    constructor(id, stationId, name, uri, type, startedAt, isEnabled, isReadOnly, canExpand) {
        this.id = id;
        this.stationId = stationId;
        this.name = name;
        this.uri = uri;
        this.type = type;
        this.startedAt = startedAt;
        this.isEnabled = isEnabled;
        this.isReadOnly = isReadOnly;
        this.canExpand = canExpand;
    }
}

module.exports = Table;