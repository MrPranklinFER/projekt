FROM node:12.13.0
# EXPOSE 3000

RUN /bin/bash -c "apt-get update --fix-missing && apt-get install -y cron && apt-get install -y curl && mkdir /usr/local/nvm"

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 12.13.0

# Install nvm with node and npm
RUN /bin/bash -c "curl https://raw.githubusercontent.com/creationix/nvm/v0.35.2/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default"

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

WORKDIR /home/app

COPY package.json /home/app/
COPY package-lock.json /home/app/

RUN /bin/bash -c "npm install"

COPY . /home/app

ADD cron.txt /etc/cron.d/projekt-cron
RUN /bin/bash -c "chmod 0644 /etc/cron.d/projekt-cron"
RUN /bin/bash -c "crontab /etc/cron.d/projekt-cron"
RUN /bin/bash -c "touch /var/log/cron.log"
CMD /bin/bash -c "npm start && cron && tail -f /var/log/cron.log"