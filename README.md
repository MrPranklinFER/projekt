# Projekt backend

## First time setup

- `docker-compose up` - initializes and starts database docker container
- `create_db.sh` - initializes the database
- `npm install` - installs dependencies required by the main program
- `npm start` - launches the main program

### Regular start

- start the db using `docker-compose up`
- start the program using `npm start`

## About main program

- Node 12.14.0
- `app.js` - entry point

## About db

- PostgreSQL 11.1
- [db schema](https://dbdiagram.io/d/5dfc60a0edf08a25543f472f)

### Credentials

**db container name:** `db`\
**user:** `backend` - has permissions `CREATEDB` and `LOGIN` \
**password:** `backend`\
**db name:** `projekt_baza`\
**port of db container:** `5433`\
**port of db inside container:** `5432`

### Scripts

- `create_db.sh` - creates database `projekt_baza` and role `backend` along with needed tables
- `drop_db.sh` - drop db `projekt_baza` and role `backend`
- `clear_db_leave_stations.sh` - clear data from all tables except `stations`

## Tips and tricks

- [**psql**](https://pgdash.io/blog/postgres-psql-tips-tricks.html)
  - `\l` - list all databases
  - `\dn` - list all schemas
  - `\dt` - list all tables
  - `\copy (<query>) to <path> csv header` - export query as .csv
  - `psql <db name> <user>` - log in into db with name \<db name\> as user \<user\>

- **docker**
  - `docker cp <src path> <container name>:/<dest path>` - copy from \<src path\> on local machine to \<dest path\> in \<container name\> container
  - `docker exec -it /bin/bash` - enter container shell
  
## Endpoints

- [Postman collection](https://www.getpostman.com/collections/41a0a7a059a407a7f797)