const { Client } = require('pg');
// I had to hardcode this because the crontab couldn't access
// variables in .env
const client = new Client({
    user: "backend",
    host: "10.5.0.2", // predefined subnet in docker-compose.yaml
    port: 5432,
    database: "projekt_baza",
    password: "backend"
});

const Station = require("../model/station");
const constants = require('../networking/constants')

module.exports.connectToDb = async function () {
    return client.connect()
        .then(console.log('connected to db'))
        .catch((e) => {
            console.error(e)
            // process.exit()
        })  
};

module.exports.closeDb = async function () {
    return client.end()
        .then(console.log('closed db connection'))
        .catch((e) => {
            console.error(e)
            process.exit()
        })
};

module.exports.select = async function (query) {
    return client.query(query)
        .catch((e) => {
            console.error(e)
            process.exit()
        })
};

module.exports.getStations = async function () {
    try {
        return client.query("SELECT * FROM stations")
            .then((res) => {
                let stations = new Array(0)
                res.rows.forEach((s) => {
                    let station = new Station(s.id, s.name, s.model, s.serial_no, s.started_at, s.ip_address)
                    stations.push(station)
                })
                return stations
            })
            .catch((e) => {
                console.error(e)
                process.exit()
            })

    }
    catch (e) {
        return console.error(e);
    }
};

module.exports.insertStation = async function (station) {
    try {
        return client.query("INSERT INTO stations (name, model, serial_no, ip_address, started_at) VALUES ($1, $2, $3, $4, to_timestamp($5)) RETURNING id", [station.name, station.model, station.serialNo, station.ipAddress, station.startedAt / 1000])
            .then((res) => {
                try { return res.rows[0].id }
                catch {
                    return undefined
                }
            })
            .catch((e) => {
                console.error(e)
                process.exit()
            })
    }
    catch (e) {
        console.error(e)
        process.exit()
    }
};

module.exports.insertTable = async function (table) {
    try {
        return client.query("INSERT INTO tables (station_id, name, started_at, is_active, collection_enabled) VALUES ($1, $2, to_timestamp($3), 1, 1) RETURNING id", [table.stationId, table.name, table.startedAt / 1000])
            .then((res) => {
                try { return res.rows[0].id }
                catch{ return undefined }
            })
            .catch((e) => {
                console.error(e)
                process.exit()
            })
    }
    catch (e) {
        console.error(e);
        process.exit()
    }
};

module.exports.getTables = async function () {
    try {
        return client.query("SELECT * FROM tables")
            .catch((e) => {
                console.error(e)
                process.exit()
            })
    }
    catch (e) {
        return console.error(e);
    }
};

module.exports.getTableIp = async function (table) {
    return client.query("SELECT ip_address FROM stations WHERE $1 = id LIMIT 1", [table.stationId])
        .then((res) => {
            return res.rows[0].ip_address
        })
        .catch((e) => {
            console.error(e)
            process.exit()
        })
};

module.exports.getTableId = async function (table) {
    return client.query("SELECT id FROM tables WHERE $1 = name", [table.name])
        .catch((e) => {
            console.error(e)
            process.exit()
        })

};

module.exports.insertField = async function (field) {
    try {
        return client.query("INSERT INTO fields (table_id, column_number, name, type, unit, process, string_len) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id", [field.tableId, field.columnNumber, field.name, field.type, field.unit, field.process, field.stringLen])
            .then((res) => {
                console.log(`Insert ${field.name} in tableId: ${field.tableId}`);
                return res.rows[0].id
            })
            .catch((e) => {
                console.error(e)
                process.exit()
            })
    }
    catch (e) {
        console.error(e);
        process.exit()
    }
}

    module.exports.checkIfStationExists = async function (station) {
        try {
            return await client.query("SELECT id FROM stations WHERE serial_no=$1", [station.serialNo])
                .then((res) => {
                    return res.rows[0].id
                }).catch((e) => {
                    console.error(e)
                    process.exit()
                })

        }
        catch (e) {
            console.error(e);
            process.exit()

        }
    }

    module.exports.getColumns = async function (tableName) {
        try {
            return client.query("SELECT name FROM fields WHERE table_id IN (SELECT id FROM tables WHERE name = $1 LIMIT 1)", [tableName])
                .then((res) => {
                    return res.rows
                })
                .catch((e) => {
                    console.error(e)
                    process.exit()
                })

        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.checkIfTableExists = async function (table) {
        try {
            return client.query("SELECT id FROM tables WHERE name = $1 AND station_id = $2", [table.name, table.stationId])
                .then((res) => {
                    try { return res.rows[0].id }
                    catch { return undefined }
                })
                .catch((e) => {
                    console.error(e)
                    process.exit()
                })

        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.checkIfFieldExists = async function (field) {
        try {
            return client.query("SELECT id, column_number FROM fields WHERE name = $1 AND table_id = $2", [field.name, field.tableId])
                .then((res) => {
                    try {
                        return Promise.resolve([res.rows[0].id, res.rows[0].column_number])
                    } catch {
                        return undefined
                    }
                })
        } catch (e) {
            console.log(e)
            process.exit(e)
        }
    }

    module.exports.insertFieldValue = async function (field, value, timestamp, sampleNumber) {
        try {
            if (typeof sampleNumber === 'undefined') {
                sampleNumber = null
            }
            return client.query("INSERT INTO sample_times (table_id, time, sample_number) VALUES ($1, $2, $3) RETURNING id", [field.tableId, timestamp, sampleNumber])
                .then(async (res) => {
                    const sampleTimeId = res.rows[0].id

                    switch (field.type) {
                        case constants.dataTypes.INTEGER:
                            return client.query("INSERT INTO samples_long (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.DOUBLE:
                            return client.query("INSERT INTO samples_float (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.FLOAT:
                            return client.query("INSERT INTO samples_float (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.STRING:
                            return client.query("INSERT INTO samples_string (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.BOOLEAN:
                            if (value == true) {
                                value = 1
                            } else {
                                value = 0
                            }
                            return client.query("INSERT INTO samples_long (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.UNSIGNED_SHORT:
                            return client.query("INSERT INTO samples_long (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.UNSIGNED_INT:
                            return client.query("INSERT INTO samples_long (field_id, sample_time_id, value) VALUES ($1, $2, $3) RETURNING id", [field.id, sampleTimeId, value])

                        case constants.dataTypes.DATE_TIME:
                            let ts = Date.parse(value) / 1000.0
                            try {
                                return client.query("INSERT INTO samples_timestamp (field_id, sample_time_id, value) VALUES ($1, $2, TO_TIMESTAMP($3)) RETURNING id", [field.id, sampleTimeId, ts]);
                            }
                            catch (e) {
                                console.log(e);
                            }
                    }
                })
        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.getFieldId = async function (field) {
        try {
            return client.query("SELECT id FROM fields WHERE name = $1 AND table_id = $2", [field.name, field.tableId])
                .then((res) => {
                    return res.rows[0].id
                })
                .catch((e) => {
                    console.error(e)
                    process.exit(e)
                })
        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.updateFieldColumnNumber = async function (field) {
        try {
            return client.query("UPDATE fields SET column_number = $1 WHERE id = $2", [field.columnNumber, field.id])
                .catch((e) => {
                    console.error(e)
                    process.exit()
                })
        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.insertStationTime = async function (station, stationTime, serverTime, responseTime) {
        try {
            stationTime = Date.parse(stationTime) / 1000.0
            serverTime = Date.parse(serverTime) / 1000.0
            client.query("INSERT INTO station_times (station_id, station_time, UTC_time, response_time) VALUES ($1, to_timestamp($2), to_timestamp($3), $4) RETURNING id", [station.id, stationTime, serverTime, responseTime])
                .then((res) => {
                    return res.rows[0].id
                })
                .catch((e) => {
                    console.error(e)
                    process.exit()
                })
        } catch (e) {
            console.error(e)
            process.exit()
        }
    }

    module.exports.getLastSampleTime = async function (table) {
        try {
            return client.query("SELECT sample_number FROM sample_times WHERE table_id = $1 ORDER BY sample_number DESC LIMIT 1", [table.id])
                .then((res) => {
                    return res.rows[0].sample_number
                })
                .catch((e) => {
                    return undefined
                })
        } catch (e) {
            console.error(e)
            process.exit()
        }
    }