const Networking = require("./networking/networking");
const constants = require("./networking/constants");
const db = require("./db/db");
const Table = require("./model/table");
const Field = require("./model/field");

const networking = new Networking();
const promises = new Array(0)

db.connectToDb().then(() => {
    doMagic()
    setTimeout(checkPromises, 10000)
})

function checkPromises() {
    Promise.all(promises).then(console.log("all done"))
    finishExecution()
}

function finishExecution() {
    db.closeDb()
    process.exit()
}

networking.on(constants.emitCodes.tableDataReceived, data => {

    let fields = data.body.head.fields;
    let receivedData = data.body.data;
    let table = data.table;
    let sinceSample = data.sinceSample;
    if (typeof sinceSample === 'undefined') {
        sinceSample = 0;
    }

    fields.forEach((f, i) => {
        let field = new Field(0, table.id, i, f.name, f.type, f.unit, f.process, f.settable, f.string_len)
        db.checkIfFieldExists(field)
            .then((data) => {
                if (typeof data !== 'undefined' && typeof data[0] !== 'undefined' && typeof data[1] !== 'undefined') {
                    let id = data[0]
                    let column_number = data[1]
                    field.id = id
                    if (field.columnNumber != column_number) {
                        field.columnNumber = column_number
                        db.updateFieldColumnNumber(field).then(() => {
                            receivedData.forEach((data) => {
                                if (data.no > sinceSample) {
                                    insertValue(field, data)
                                }
                            })})
                    } else {
                        receivedData.forEach((data) => {
                            if (data.no > sinceSample) {
                                insertValue(field, data)
                            }
                        })
                    }
                } else {
                    db.insertField(field)
                        .then((id) => {
                            field.id = id
                            receivedData.forEach((data) => {
                                insertValue(field, data)
                            })
                        })
                }
            })
    });
});

networking.on(constants.emitCodes.tablesReceived, data => {
    data.body.symbols.forEach(value => {
        let table = new Table(0, data.station.id, value.name, value.uri, value.type, Date.now(), value.is_enabled, value.is_read_only, value.can_expand);

        db.checkIfTableExists(table)
            .then((id) => {
                if (typeof id !== 'undefined') {
                    console.log(`table ${table.name} already exists`)
                    table.id = id
                    db.getLastSampleTime(table)
                        .then((sample_no) => {
                            getTableData(table, sample_no)
                        })
                } else {
                    db.insertTable(table).then((id) => {
                        table.id = id
                        getTableData(table, null)
                    })
                }
            })
    });
});

networking.on(constants.emitCodes.stationInfoReceived, data => {
    let station = data.station
    db.checkIfStationExists(station)
        .then((id) => {
            if (typeof id !== 'undefined') {
                console.log(`station ${station.name} already exists`)
                station.id = id
                networking.getListOfTables(station)
            } else {
                db.insertStation(station)
                    .then((id) => {
                        station.id = id
                        networking.getListTables(station)
                    })
            }
        })
})

networking.on(constants.emitCodes.stationTimeReceived, data => {
    let station = data.station
    let responseTime = data.responseTime
    let requestTime = data.requestTime
    let stationTime = data.body.time
    let outcome = data.body.outcome
    let description = data.body.description

    if (outcome == 1) {
        db.insertStationTime(station, stationTime, requestTime, responseTime)
    } else {
        console.error(description)
    }

    console.log(`${station.name}: ${description} - ${stationTime}`)
})

async function doMagic() {
    db.getStations()
        .then((stations) => {
            stations.forEach((station) => {
                networking.getStationInfo(station.ipAddress, {
                    command: "dataquery",
                    uri: "Status",
                    mode: "most-recent",
                    format: "json"
                })
                networking.getStationTime(station)
            })
        })
}

function getTableData(table, sinceSample) {
    db.getTableIp(table).then(ip => {
        const ipAddress = ip
        networking.getDataFromTable(table, ipAddress, sinceSample);
    })
}

function insertValue(field, receivedData) {
    console.log(`inserting value to ${field.name} from table id ${field.tableId}`)
    let value = receivedData.vals[field.columnNumber]
    let timestamp = new Date(receivedData.time)
    let sampleNo = receivedData.no
    promises.push(new Promise((resolve, reject) => {
        db.insertFieldValue(field, value, timestamp, sampleNo)
            .then(resolve("pass"))
            .catch((e) => reject(e))
    }))
}